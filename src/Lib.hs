module Lib
    ( someFunc
    ) where

import qualified Network.Wreq

someFunc :: IO ()
someFunc = putStrLn "someFunc"
